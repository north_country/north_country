package com.tallboys.northcountry.components

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.Json
import com.tallboys.northcountry.Entity
import com.tallboys.northcountry.MapManager
import com.tallboys.northcountry.Utility

abstract class GraphicsComponent: ComponentSubject(), Component {
    companion object {
        private const val TAG = "GRAPHICS COMPONENT"
    }
    var currentFrame = TextureRegion()
    var frameTime = 0f
    var currentState = Entity.State.WALKING
    var currentDirection = Entity.Direction.DOWN
    var currentPosition = Vector2()
    val animations = HashMap<Entity.AnimationType, Animation<TextureRegion>>()
    var shapeRenderer = ShapeRenderer()
    val json = Json()

    abstract fun update(entity: Entity, mapManager: MapManager, batch: Batch, delta: Float)

    fun updateAnimations(delta: Float) {
        frameTime = (frameTime + delta)%5

        //todo needs to be refactored and cleaned up
        when (currentState) {
            Entity.State.RUNNING -> {
                when (currentDirection) {
                    Entity.Direction.DOWN -> {
                        setAnimation(Entity.State.RUNNING, Entity.AnimationType.RUN_DOWN)
                    }
                    Entity.Direction.UP -> {
                        setAnimation(Entity.State.RUNNING, Entity.AnimationType.RUN_UP)
                    }
                    Entity.Direction.LEFT -> {
                        setAnimation(Entity.State.RUNNING, Entity.AnimationType.RUN_LEFT)
                    }
                    Entity.Direction.RIGHT -> {
                        setAnimation(Entity.State.RUNNING, Entity.AnimationType.RUN_RIGHT)
                    }
                }
            }
            else -> {
                when (currentDirection) {
                    Entity.Direction.DOWN -> {
                        setAnimation(Entity.State.WALKING, Entity.AnimationType.WALK_DOWN)
                    }
                    Entity.Direction.UP -> {
                        setAnimation(Entity.State.WALKING, Entity.AnimationType.WALK_UP)
                    }
                    Entity.Direction.LEFT -> {
                        setAnimation(Entity.State.WALKING, Entity.AnimationType.WALK_LEFT)
                    }
                    Entity.Direction.RIGHT -> {
                        setAnimation(Entity.State.WALKING, Entity.AnimationType.WALK_RIGHT)
                    }
                }
            }
        }


    }

    // todo these can be refactored
    fun loadAnimation(firstTexture: String, secondTexture: String, points: Array<GridPoint2>, frameDuration: Float): Option<Animation<TextureRegion>> {
        Utility.loadTextureAsset(firstTexture)

        when (val texture1 = Utility.getTextureAsset(firstTexture)) {
            is Some -> {
                Utility.loadTextureAsset(secondTexture)
                when (val texture2 = Utility.getTextureAsset(secondTexture)) {
                    is Some -> {
                        val texture1Frames = TextureRegion.split(texture1.value,
                            Entity.FRAME_WIDTH,
                            Entity.FRAME_HEIGHT
                        )
                        val texture2Frames = TextureRegion.split(texture2.value,
                            Entity.FRAME_WIDTH,
                            Entity.FRAME_HEIGHT
                        )

                        val animationKeyFrames = Array<TextureRegion>(2)

                        val point = points.first()

                        animationKeyFrames.add(texture1Frames[point.x][point.y])
                        animationKeyFrames.add(texture2Frames[point.x][point.y])

                        val animation = Animation(frameDuration, animationKeyFrames)

                        animation.playMode = Animation.PlayMode.LOOP

                        return Some(animation)
                    }
                    is None -> Gdx.app.debug(TAG, "No texture 2")
                }
            }
            is None -> Gdx.app.debug(TAG, "No texture 1")
        }
        return None
    }

    fun loadAnimation(textureName: String, points: Array<GridPoint2>, frameDuration: Float): Option<Animation<TextureRegion>> {
        Utility.loadTextureAsset(textureName)

        when (val texture = Utility.getTextureAsset(textureName)) {
            is Some -> {
                val textureFrames = TextureRegion.split(texture.value, Entity.FRAME_WIDTH, Entity.FRAME_HEIGHT)
                val animationKeyFrames = Array<TextureRegion>(points.size)

                for (point in points) {
                    animationKeyFrames.add(textureFrames[point.x][point.y])
                }

                val animation = Animation(frameDuration, animationKeyFrames)
                animation.playMode = Animation.PlayMode.LOOP

                return Some(animation)
            }
            is None -> {
                Gdx.app.debug(TAG, "Unable to load texture: $textureName")
                return None
            }
        }
    }

    private fun setAnimation(state: Entity.State, animationType: Entity.AnimationType) {
        // todo all of this is terrible
        val animation = if (currentState == Entity.State.WALKING || currentState == Entity.State.RUNNING) {
            animations[animationType] ?: return
        } else if (currentState == Entity.State.IDLE){
            when (currentDirection) {
                Entity.Direction.LEFT -> animations[Entity.AnimationType.IDLE_LEFT] ?: return
                Entity.Direction.RIGHT -> animations[Entity.AnimationType.IDLE_RIGHT] ?: return
                Entity.Direction.DOWN -> animations[Entity.AnimationType.IDLE_DOWN] ?: return
                Entity.Direction.UP -> animations[Entity.AnimationType.IDLE_UP] ?: return
            }
        } else {
            animations[Entity.AnimationType.IMMOBILE] ?: return
        }
        currentFrame = animation.getKeyFrame(frameTime)
    }
}