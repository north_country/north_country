package com.tallboys.northcountry.maps

import arrow.core.None
import arrow.core.Some
import arrow.core.toOption

object MapFactory {
    enum class MapType {
        Map1,
        HouseTest
    }

    private val maps = HashMap<MapType, Map>(MapType.values().size)

    fun getMap(type: MapType): Map {
        return when (type) {
            MapType.Map1 -> {
                when (val map = maps[MapType.Map1]
                    .toOption()) {
                    is Some -> map.value
                    is None -> {
                        val newMap = TestMap()
                        maps[MapType.Map1] = newMap
                        newMap
                    }
                }
            }
            MapType.HouseTest -> {
                when (val map = maps[MapType.HouseTest].toOption()) {
                    is Some -> map.value
                    is None -> {
                        val newMap = TestHouse()
                        maps[MapType.HouseTest] = newMap
                        newMap
                    }
                }
            }
        }
    }

    fun clearCache() {
        maps.values.forEach { it.dispose() }
        maps.clear()
    }
}