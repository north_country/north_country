package com.tallboys.northcountry.maps

class TestHouse: Map(MapFactory.MapType.HouseTest, path) {
    companion object {
        private const val TAG = "TEST MAP"
        private const val path = "maps/MapTest/HouseTest.tmx"
    }
}