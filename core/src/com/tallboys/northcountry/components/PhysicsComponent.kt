package com.tallboys.northcountry.components

import arrow.core.None
import arrow.core.Some
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.maps.objects.RectangleMapObject
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.tallboys.northcountry.Entity
import com.tallboys.northcountry.MapManager
import com.tallboys.northcountry.maps.Map

abstract class PhysicsComponent: ComponentSubject(), Component {
    enum class BoundingBoxLocation {
        BOTTOM_LEFT,
        BOTTOM_CENTER,
        CENTER
    }

    companion object {
        private const val TAG = "PHYSICS COMPONENT"
    }

    val nextEntityPosition = Vector2(0f, 0f)
    var currentEntityPosition = Vector2(0f, 0f)
    val walkVelocity = Vector2(7f, 7f)
    val runVelocity = Vector2(12f, 12f)
    val boundingBox = Rectangle()
    var boundingBoxLocation = BoundingBoxLocation.BOTTOM_LEFT
    var currentDirection = Entity.Direction.DOWN
    var state = Entity.State.IDLE

    abstract fun update(entity: Entity, mapMgr: MapManager, delta: Float)

    fun isCollisionWithMapLayer(entity: Entity, mapMgr: MapManager): Boolean {
        return when (val mapCollisionLayer = mapMgr.map.getCollisionLayer()) {
            is Some -> {
                var isCollision = false
                mapCollisionLayer.value.objects.forEach {
                    if (it is RectangleMapObject) {
                        if (boundingBox.overlaps(it.rectangle)) {
                            entity.sendMessage(Component.MESSAGE.COLLISION_WITH_MAP)
                            isCollision = true
                        }
                    }
                }
                isCollision
            }
            is None -> false
        }
    }

    fun setNextPositionToCurrent(entity: Entity) {
        currentEntityPosition.set(nextEntityPosition.cpy())
        //Gdx.app.debug(TAG, "CURRENT POSITION (${currentEntityPosition.x}, ${currentEntityPosition.y})")
        entity.sendMessage(Component.MESSAGE.CURRENT_POSITION, currentEntityPosition.x.toString(), currentEntityPosition.y.toString())
    }

    fun calculateNextPosition(deltaTime: Float) {
        if (deltaTime > .7) return

        var testX = currentEntityPosition.x
        var testY = currentEntityPosition.y

        walkVelocity.scl(deltaTime)
        runVelocity.scl(deltaTime)

        val velocity = if (state == Entity.State.RUNNING) runVelocity
        else walkVelocity
        when (currentDirection) {
            Entity.Direction.LEFT -> testX -= velocity.x
            Entity.Direction.RIGHT -> testX += velocity.x
            Entity.Direction.UP -> testY += velocity.y
            Entity.Direction.DOWN -> testY -= velocity.y
        }

        nextEntityPosition.x = testX
        nextEntityPosition.y = testY

        //Gdx.app.debug(TAG, "TESTY $testY")

        walkVelocity.scl(1 / deltaTime)
        runVelocity.scl(1/ deltaTime)
    }

    fun initBoundingBox(percentageWidthReduced: Float, percentageHeightReduced: Float) {

        val widthReductionAmount = 1.0f - percentageWidthReduced
        val heightReductionAmount = 1.0f - percentageHeightReduced

        val width = if (widthReductionAmount > 0 && widthReductionAmount < 1) Entity.FRAME_WIDTH * widthReductionAmount
        else Entity.FRAME_WIDTH.toFloat()

        val height = if (heightReductionAmount > 0 && heightReductionAmount < 1) (Entity.FRAME_HEIGHT * heightReductionAmount)
        else Entity.FRAME_HEIGHT.toFloat()

        if (width == 0f || height == 0f) {
            Gdx.app.debug(TAG, "Width and Height are 0!! ($width, $height)")
        }

        val (minX, minY) = if (Map.UNIT_SCALE > 0) {
            Pair(nextEntityPosition.x / Map.UNIT_SCALE, nextEntityPosition.y / Map.UNIT_SCALE)
        } else Pair(nextEntityPosition.x, nextEntityPosition.y)

        boundingBox.setWidth(width)
        boundingBox.setHeight(height)

        when (boundingBoxLocation) {
            BoundingBoxLocation.BOTTOM_LEFT -> boundingBox.set(minX, minY, width, height)
            BoundingBoxLocation.BOTTOM_CENTER -> boundingBox.setCenter(minX + Entity.FRAME_WIDTH /2,
                minY + Entity.FRAME_HEIGHT /4)
            BoundingBoxLocation.CENTER -> boundingBox.setCenter(minX + Entity.FRAME_WIDTH /2,
                minY + Entity.FRAME_HEIGHT /2)
        }
    }

    fun updateBoundingBoxPosition(position: Vector2) {

        val (minX, minY) = if (Map.UNIT_SCALE > 0) Pair(position.x / Map.UNIT_SCALE, position.y / Map.UNIT_SCALE)
        else Pair(position.x, position.y)

        when (boundingBoxLocation) {
            BoundingBoxLocation.BOTTOM_LEFT -> boundingBox.set(minX, minY,
                boundingBox.getWidth(),
                boundingBox.getHeight())
            BoundingBoxLocation.BOTTOM_CENTER -> boundingBox.setCenter(minX + Entity.FRAME_WIDTH /2,
                minY + Entity.FRAME_HEIGHT /4)
            BoundingBoxLocation.CENTER -> boundingBox.setCenter(minX + Entity.FRAME_WIDTH /2,
                minY + Entity.FRAME_HEIGHT /2)
        }
    }
}