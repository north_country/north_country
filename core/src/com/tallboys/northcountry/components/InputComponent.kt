package com.tallboys.northcountry.components

import arrow.core.None
import arrow.core.Option
import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.utils.Json
import com.tallboys.northcountry.Entity

abstract class InputComponent: ComponentSubject(), Component, InputProcessor {
    enum class Keys {
        LEFT, RIGHT, UP, DOWN, QUIT, PAUSE, SHIFT, E
    }

    companion object {
        val keys = HashMap<Keys, Boolean>()

        init {
            keys[Keys.LEFT] = false
            keys[Keys.RIGHT] = false
            keys[Keys.UP] = false
            keys[Keys.DOWN] = false
            keys[Keys.QUIT] = false
            keys[Keys.SHIFT] = false
            keys[Keys.E] = false
        }
    }
    var currentDirection: Option<Entity.Direction> = None
    var currentState: Option<Entity.State> = None
    val json = Json()

    abstract fun update(entity: Entity, delta: Float)
}