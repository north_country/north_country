package com.tallboys.northcountry.maps

class TestMap: Map(MapFactory.MapType.Map1, path) {
    companion object {
        private const val TAG = "TEST MAP"
        private const val path = "maps/MapTest/Map1.tmx"
    }
}