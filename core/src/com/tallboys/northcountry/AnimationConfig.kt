package com.tallboys.northcountry

import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.utils.Array

/**
 * Can't use a data class because reflection is gross
 */
class AnimationConfig() {
    var frameDuration: Float = 0.0f
    var animationType: Entity.AnimationType = Entity.AnimationType.WALK_UP
    var texturePath: String = ""
    var gridPoints: Array<GridPoint2> = Array()
}