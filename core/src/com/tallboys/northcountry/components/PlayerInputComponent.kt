package com.tallboys.northcountry.components

import arrow.core.Some
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.tallboys.northcountry.Entity
import com.tallboys.northcountry.components.Component.Companion.MESSAGE_TOKEN

class PlayerInputComponent: InputComponent() {
    companion object {
        private const val TAG = "PLAYER INPUT COMPONENT"
    }

    fun clear() {
        keys.keys.forEach {
            keys[it] = false
        }
    }

    override fun update(entity: Entity, delta: Float) {
        if (keys[Keys.SHIFT] == true) {
            if (keys[Keys.LEFT] == true) {
                updateStateAndDir(entity, Entity.State.RUNNING, Entity.Direction.LEFT)
            } else if (keys[Keys.RIGHT] == true) {
                updateStateAndDir(entity, Entity.State.RUNNING, Entity.Direction.RIGHT)
            } else if (keys[Keys.UP] == true) {
                updateStateAndDir(entity, Entity.State.RUNNING, Entity.Direction.UP)
            } else if (keys[Keys.DOWN] == true) {
                updateStateAndDir(entity, Entity.State.RUNNING, Entity.Direction.DOWN)
            }
        } else if (keys[Keys.LEFT] == true) {
            updateStateAndDir(entity, Entity.State.WALKING, Entity.Direction.LEFT)
        } else if (keys[Keys.RIGHT] == true) {
            updateStateAndDir(entity, Entity.State.WALKING, Entity.Direction.RIGHT)
        } else if (keys[Keys.UP] == true) {
            updateStateAndDir(entity, Entity.State.WALKING, Entity.Direction.UP)
        } else if (keys[Keys.DOWN] == true) {
            updateStateAndDir(entity, Entity.State.WALKING, Entity.Direction.DOWN)
        } else if (keys[Keys.E] == true) {
            updateInteractive(entity)
            // dont want to spam E
            eReleased()
        } else {
            updateStateAndDir(entity, Entity.State.IDLE, currentDirection.orNull() ?: Entity.Direction.DOWN)
        }
    }

    override fun dispose() {
        Gdx.input.inputProcessor = null
    }

    override fun receiveMessage(message: String) {
        val string = message.split(MESSAGE_TOKEN)
        if (string.isEmpty()) return

        if (string[0] == Component.MESSAGE.CURRENT_DIRECTION.toString()) {
            currentDirection = Some(Entity.Direction.valueOf(string[1]))
        }
    }

    override fun keyDown(keycode: Int): Boolean {
        if (keycode == Input.Keys.LEFT || keycode == Input.Keys.A) {
            leftPressed()
        }
        if (keycode == Input.Keys.RIGHT || keycode == Input.Keys.D) {
            rightPressed()
        }
        if (keycode == Input.Keys.UP || keycode == Input.Keys.W) {
            upPressed()
        }
        if (keycode == Input.Keys.DOWN || keycode == Input.Keys.S) {
            downPressed()
        }
        if (keycode == Input.Keys.SHIFT_LEFT || keycode == Input.Keys.SHIFT_RIGHT) {
            shiftPressed()
        }
        if (keycode == Input.Keys.E) {
            ePressed()
        }
        return true
    }

    override fun keyUp(keycode: Int): Boolean {
        if (keycode == Input.Keys.LEFT || keycode == Input.Keys.A) {
            leftReleased()
        }
        if (keycode == Input.Keys.RIGHT || keycode == Input.Keys.D) {
            rightReleased()
        }
        if (keycode == Input.Keys.UP || keycode == Input.Keys.W) {
            upReleased()
        }
        if (keycode == Input.Keys.DOWN || keycode == Input.Keys.S) {
            downReleased()
        }
        if (keycode == Input.Keys.SHIFT_LEFT || keycode == Input.Keys.SHIFT_RIGHT) {
            shiftReleased()
        }
        if (keycode == Input.Keys.E) {
            eReleased()
        }
        return true
    }

    override fun keyTyped(character: Char): Boolean {
        return false
    }

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return false
    }

    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return false
    }

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        return false
    }

    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        return false
    }

    override fun scrolled(amountX: Float, amountY: Float): Boolean {
        return false
    }

    fun leftPressed() { keys[Keys.LEFT] = true }
    fun rightPressed() { keys[Keys.RIGHT] = true }
    fun upPressed() { keys[Keys.UP] = true }
    fun downPressed() { keys[Keys.DOWN] = true }
    fun shiftPressed() { keys[Keys.SHIFT] = true }
    fun ePressed() { keys[Keys.E] = true }
    fun leftReleased() { keys[Keys.LEFT] = false }
    fun rightReleased() { keys[Keys.RIGHT] = false }
    fun upReleased() { keys[Keys.UP] = false }
    fun downReleased() { keys[Keys.DOWN] = false }
    fun shiftReleased() { keys[Keys.SHIFT] = false }
    fun eReleased() { keys[Keys.E] = false }

    private fun updateStateAndDir(entity: Entity, state: Entity.State, dir: Entity.Direction) {
        entity.sendMessage(Component.MESSAGE.CURRENT_STATE, state.toString())
        entity.sendMessage(Component.MESSAGE.CURRENT_DIRECTION, dir.toString())
    }

    private fun updateInteractive(entity: Entity) {
        entity.sendMessage(Component.MESSAGE.COLLISION_WITH_INTERACTIVE)
    }
}