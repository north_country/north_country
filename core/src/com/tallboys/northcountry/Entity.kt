package com.tallboys.northcountry

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.utils.Array
import com.tallboys.northcountry.components.Component
import com.tallboys.northcountry.components.GraphicsComponent
import com.tallboys.northcountry.components.InputComponent
import com.tallboys.northcountry.components.PhysicsComponent

class Entity(
    val inputComponent: InputComponent,
    val physicsComponent: PhysicsComponent,
    val graphicsComponent: GraphicsComponent
) {
    enum class Direction {
        UP, RIGHT, DOWN, LEFT
    }

    enum class State {
        IDLE, WALKING, IMMOBILE, RUNNING
    }

    enum class AnimationType {
        WALK_LEFT,
        WALK_RIGHT,
        WALK_UP,
        WALK_DOWN,
        IDLE_LEFT,
        IDLE_RIGHT,
        IDLE_UP,
        IDLE_DOWN,
        IMMOBILE,
        RUN_LEFT,
        RUN_RIGHT,
        RUN_UP,
        RUN_DOWN
    }

    companion object {
        const val FRAME_WIDTH = 32
        const val FRAME_HEIGHT = 48
    }

    val components = Array<Component>(3)

    init {
        components.add(inputComponent)
        components.add(physicsComponent)
        components.add(graphicsComponent)
    }

    fun sendMessage(messageType: Component.MESSAGE, vararg args: String) {
        var fullMessage = messageType.toString()

        for (string in args) {
            fullMessage += Component.MESSAGE_TOKEN + string
        }

        for (component in components) {
            component.receiveMessage(fullMessage)
        }
    }

    fun update(mapMgr: MapManager, batch: Batch, delta: Float) {
        inputComponent.update(this, delta)
        physicsComponent.update(this, mapMgr, delta)
        graphicsComponent.update(this, mapMgr, batch, delta)
    }
}