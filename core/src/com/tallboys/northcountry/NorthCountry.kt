package com.tallboys.northcountry

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import com.badlogic.gdx.Game
import com.tallboys.northcountry.screens.MainGameScreen

class NorthCountry: Game() {
    companion object {
        private var mainGameScreen: Option<MainGameScreen> = None
    }

    override fun create() {
        val mg = MainGameScreen(this)
        mainGameScreen = Some(mg)
        setScreen(mg)
    }

    override fun dispose() {
        when (val mg = mainGameScreen)
        { is Some -> mg.value.dispose() }
    }
}