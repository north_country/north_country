package com.tallboys.northcountry

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import arrow.core.toOption
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.maps.objects.RectangleMapObject
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.utils.Array
import com.tallboys.northcountry.interactable.Crop
import com.tallboys.northcountry.interactable.Interactive
import com.tallboys.northcountry.maps.Map
import com.tallboys.northcountry.maps.Map.Companion.UNIT_SCALE
import com.tallboys.northcountry.maps.MapFactory
import com.tallboys.northcountry.maps.TestMap
import kotlin.math.abs

class MapManager(val camera: OrthographicCamera) {
    companion object {
        private const val TAG = "MAP MANAGER"
    }

    enum class MapMessage {
        HARVEST_CROP
    }

    var map: Map = TestMap()
    var mapChanged = true
    var previousMapName = ""

    fun loadMap(mapType: MapFactory.MapType) {
        previousMapName = this.map.type.name
        this.map = MapFactory.getMap(mapType)

        this.mapChanged = true
    }

    fun getPlayerStart(): Option<Rectangle> {
        when (val spawn = map.getSpawnLayer()) {
            is Some -> {
                val spawnStart = if (previousMapName.isNotEmpty()) "${Map.PLAYER_START}:$previousMapName"
                else Map.PLAYER_START
                Gdx.app.debug(TAG, "SPAWN START: $spawnStart")
                val playerStart = spawn.value.objects.find { it.name == spawnStart }
                    .toOption()
                return when(playerStart) {
                    is Some -> Some((playerStart.value as RectangleMapObject).rectangle)
                    is None -> None
                }
            }
        }
        return None
    }

    fun getInteractiveObjects(): Array<Interactive> {
        val interactiveObj = Array<Interactive>()
        when (val interactives = map.getInteractLayer()) {
            is Some -> {
                interactives.value.objects.forEach {
                    if (it is RectangleMapObject) {
                        when (Interactive.Type.valueOf(it.name)) {
                            Interactive.Type.CROP -> {
                                interactiveObj.add(Crop(it.rectangle, this))
                            }
                        }
                    }
                }
            }
        }
        return interactiveObj
    }

    fun notify(message: MapMessage, interactive: Interactive, command: String) {
        when (message) {
            MapMessage.HARVEST_CROP -> {
                when (val dl = map.getDecorationLayer()) {
                    is Some -> {
                        val tomatoAtlas = Utility.VEGGIE_ATLAS.findRegion("tomato")
                        val tomatoCell = TiledMapTileLayer.Cell()
                        tomatoCell.tile = StaticTiledMapTile(TextureRegion(tomatoAtlas))
                        val x = (interactive.rectangle.x.toInt() / 32)
                        val y = (interactive.rectangle.y.toInt() / 32)
                        (dl.value as TiledMapTileLayer).setCell(x, y, tomatoCell)
                    }
                }
            }
        }
    }
}