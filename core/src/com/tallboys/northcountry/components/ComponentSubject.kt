package com.tallboys.northcountry.components

import com.badlogic.gdx.utils.Array

open class ComponentSubject {
    val observers = Array<ComponentObserver>()

    fun addObserver(conversationObserver: ComponentObserver) {
        observers.add(conversationObserver)
    }

    fun removeObserver(conversationObserver: ComponentObserver) {
        observers.removeValue(conversationObserver, true)
    }

    fun removeAllObservers() {
        observers.removeAll(observers, true)
    }

    fun notify(value: String, event: ComponentObserver.ComponentEvent) {
        observers.forEach { it.onNotify(value, event) }
    }
}