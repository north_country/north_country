package com.tallboys.northcountry.maps

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import arrow.core.toOption
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.maps.MapLayer
import com.badlogic.gdx.maps.objects.RectangleMapObject
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.tallboys.northcountry.Utility
import kotlin.system.exitProcess

abstract class Map(val type: MapFactory.MapType, path: String) {
    companion object {
        const val UNIT_SCALE = 1/16f

        const val BACKGROUND_LAYER = "Background"
        const val GROUND_LAYER = "Ground"
        const val DECORATION_LAYER = "Decoration"
        const val TOP_LAYER = "TopDecoration"
        const val COLLISION_LAYER = "MapCollision"
        const val SPAWN_LAYER = "MapSpawn"
        const val INTERACT_LAYER = "MapInteract"

        const val PLAYER_START = "PlayerStart"
    }

    private val TAG = type.name
    val map: TiledMap
    var cachedStart: Option<Rectangle> = None

    init {
        if (path.isEmpty()) {
            Gdx.app.debug(TAG, "Map file path is empty")
            Gdx.app.exit()
        }

        Utility.loadMapAsset(path)
        when (val m = Utility.getMapAsset(path)) {
            is Some -> {
                map = m.value
            }
            is None -> {
                Gdx.app.debug(TAG, "Unable to get map $path")
                Gdx.app.exit()
                // todo this is gross
                exitProcess(1)
            }
        }
    }

    fun getBackgroundLayer(): Option<MapLayer> {
        return map.layers[BACKGROUND_LAYER].toOption()
    }

    fun getGroundLayer(): Option<MapLayer> {
        return map.layers[GROUND_LAYER].toOption()
    }

    fun getDecorationLayer(): Option<MapLayer> {
        return map.layers[DECORATION_LAYER].toOption()
    }

    fun getTopLayer(): Option<MapLayer> {
        return map.layers[TOP_LAYER].toOption()
    }

    fun getCollisionLayer(): Option<MapLayer> {
        return map.layers[COLLISION_LAYER].toOption()
    }

    fun getSpawnLayer(): Option<MapLayer> {
        return map.layers[SPAWN_LAYER].toOption()
    }

    fun getInteractLayer(): Option<MapLayer> {
        return map.layers[INTERACT_LAYER].toOption()
    }

    fun dispose() {

    }
}