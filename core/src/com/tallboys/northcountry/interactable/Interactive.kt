package com.tallboys.northcountry.interactable

import com.badlogic.gdx.math.Rectangle

abstract class Interactive(val rectangle: Rectangle) {

    enum class Type {
        CROP
    }
    abstract fun interact()
}