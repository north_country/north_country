package com.tallboys.northcountry

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetLoaderParameters
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader
import com.badlogic.gdx.assets.loaders.MusicLoader
import com.badlogic.gdx.assets.loaders.SoundLoader
import com.badlogic.gdx.assets.loaders.TextureLoader
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TmxMapLoader
import com.badlogic.gdx.utils.Disposable

object Utility {
    private const val TAG = "UTILITY"
    private val assetManager = AssetManager()
    private val filePathResolver = InternalFileHandleResolver()

    private val VEGGIE_ATLAS_PATH = "sprites/atlas/veggies.atlas"

    val VEGGIE_ATLAS = TextureAtlas(VEGGIE_ATLAS_PATH)


    fun isAssetLoaded(fileName: String): Boolean {
        return assetManager.isLoaded(fileName)
    }

    fun loadMapAsset(mapName: String): Boolean {
        return loadAsset(TiledMap::class.java, mapName, TmxMapLoader(filePathResolver))
    }

    fun loadSoundAsset(soundName: String): Boolean {
        return loadAsset(Sound::class.java, soundName, SoundLoader(filePathResolver))
    }

    fun loadMusicAsset(musicName: String): Boolean {
        return loadAsset(Music::class.java, musicName, MusicLoader(filePathResolver))
    }

    fun loadTextureAsset(textureName: String): Boolean {
        return loadAsset(Texture::class.java, textureName, TextureLoader(filePathResolver))
    }

    fun getMapAsset(mapName: String): Option<TiledMap> {
        return getAsset(TiledMap::class.java, mapName)
    }

    fun getSoundAsset(soundName: String): Option<Sound> {
        return getAsset(Sound::class.java, soundName)
    }

    fun getMusicAsset(musicName: String): Option<Music> {
        return getAsset(Music::class.java, musicName)
    }

    fun getTextureAsset(textureName: String): Option<Texture> {
        return getAsset(Texture::class.java, textureName)
    }

    private fun <T: Disposable,
            B: AssetLoaderParameters<T>,
            P: AsynchronousAssetLoader<T, B>>
            loadAsset(type: Class<T>, name: String, asset: P): Boolean {
        if (name.isEmpty()) {
            Gdx.app.debug(TAG, "The asset name is empty")
            return false
        }
        if (!filePathResolver.resolve(name).exists()) {
            Gdx.app.debug(TAG, "The file $name does not exist")
            return false
        }

        assetManager.setLoader(type, asset)
        assetManager.load(name, type)
        // todo this will block until loaded
        //  for now it is whatever, but we will probably
        //  want to async load and use a progress bar
        assetManager.finishLoadingAsset<TiledMap>(name)

        Gdx.app.debug(TAG, "$name loaded")
        return true
    }

    private fun <T: Disposable> getAsset(type: Class<T>, name: String): Option<T> {
        return if (assetManager.isLoaded(name)) {
            Some(assetManager.get(name, type))
        } else {
            Gdx.app.debug(TAG, "Asset $name is not loaded")
            None
        }
    }


}