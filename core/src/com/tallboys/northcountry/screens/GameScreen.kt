package com.tallboys.northcountry.screens

import com.badlogic.gdx.Screen

open class GameScreen: Screen {
    override fun show() {

    }

    override fun render(delta: Float) {

    }

    override fun resize(width: Int, height: Int) {

    }

    override fun pause() {

    }

    override fun resume() {

    }

    override fun hide() {

    }

    override fun dispose() {

    }
}