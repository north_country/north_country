package com.tallboys.northcountry.screens

import arrow.core.None
import arrow.core.Some
import arrow.core.or
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.maps.objects.TextureMapObject
import com.badlogic.gdx.maps.tiled.TiledMapTile
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.tallboys.northcountry.*
import com.tallboys.northcountry.components.Component
import com.tallboys.northcountry.components.PlayerGraphicsComponent
import com.tallboys.northcountry.components.PlayerInputComponent
import com.tallboys.northcountry.components.PlayerPhysicsComponent
import com.tallboys.northcountry.maps.Map.Companion.UNIT_SCALE
import com.tallboys.northcountry.maps.MapFactory

class MainGameScreen(val game: NorthCountry): GameScreen() {
    object ViewPort {
        var viewPortWidth = 0f
        var viewPortHeight = 0f
        var virtualWidth = 0f
        var virtualHeight = 0f
        var physicalWidth = 0f
        var physicalHeight = 0f
        var aspectRatio = 0f
    }

    companion object {
        private const val TAG = "MAIN GAME SCREEN"
    }
    val camera = OrthographicCamera()
    val mapManager = MapManager(camera)
    val mapRenderer = OrthogonalTiledMapRenderer(mapManager.map.map, UNIT_SCALE)
    val player = Entity(PlayerInputComponent(), PlayerPhysicsComponent(), PlayerGraphicsComponent())
    val multiplexer = InputMultiplexer()
    var firstRun = true //todo testing
    init {
        // todo this is for testing
        player.sendMessage(Component.MESSAGE.LOAD_ANIMATIONS)

        setupViewport(30f, 30f)
        camera.setToOrtho(false, ViewPort.viewPortWidth, ViewPort.viewPortHeight)
        // todo testing only

        multiplexer.addProcessor(player.inputComponent)
        Gdx.input.inputProcessor = multiplexer
    }

    override fun render(delta: Float) {
        val map = mapManager.map
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        mapRenderer.setView(camera)
        mapRenderer.batch.enableBlending()
        mapRenderer.batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)

        if (mapManager.mapChanged) {
            mapRenderer.map = map.map
            when (val rect = mapManager.getPlayerStart()) {
                is Some -> {
                    player.sendMessage(Component.MESSAGE.INIT_START_POSITION, (rect.value.x * UNIT_SCALE).toString(), (rect.value.y * UNIT_SCALE).toString())
                }
                is None -> {
                    Gdx.app.debug(TAG, "MAP ${map.type} HAS NO PLAYER SPAWN!")
                }
            }
            when (val dl = map.getDecorationLayer()) {
                is Some -> {
                    val tomatoAtlas = Utility.VEGGIE_ATLAS.findRegion("tomato")
                    val tomatoCell = TiledMapTileLayer.Cell()
                    tomatoCell.tile = StaticTiledMapTile(TextureRegion(tomatoAtlas))
//                    tomatoCell.flipHorizontally = false
//                    tomatoCell.flipVertically = false
//                    tomatoCell.rotation = 0
                    (dl.value as TiledMapTileLayer).setCell(20, 10, tomatoCell)
                }
            }
            mapManager.mapChanged = false
        }

        mapRenderer.batch.begin()
        when (val bg = map.getBackgroundLayer())
        { is Some -> mapRenderer.renderTileLayer(bg.value as TiledMapTileLayer) }
        when (val gl = map.getGroundLayer())
        { is Some -> mapRenderer.renderTileLayer(gl.value as TiledMapTileLayer) }
        when (val dl = map.getDecorationLayer())
        { is Some -> mapRenderer.renderTileLayer(dl.value as TiledMapTileLayer) }
        mapRenderer.batch.end()

        player.update(mapManager, mapRenderer.batch, delta)

        mapRenderer.batch.begin()
        when (val tl = map.getTopLayer())
        { is Some -> mapRenderer.renderTileLayer(tl.value as TiledMapTileLayer)}
        mapRenderer.batch.end()
    }

    override fun dispose() {
        mapRenderer.dispose()
        MapFactory.clearCache()
    }

    private fun setupViewport(width: Float, height: Float) {
        ViewPort.virtualWidth = width
        ViewPort.virtualHeight = height
        ViewPort.viewPortWidth = ViewPort.virtualWidth
        ViewPort.viewPortHeight = ViewPort.virtualHeight
        ViewPort.physicalWidth = Gdx.graphics.width.toFloat()
        ViewPort.physicalHeight = Gdx.graphics.height.toFloat()
        ViewPort.aspectRatio = (ViewPort.virtualWidth / ViewPort.virtualHeight)
        if (ViewPort.physicalWidth / ViewPort.physicalHeight >= ViewPort.aspectRatio) {
            ViewPort.viewPortWidth = ViewPort.viewPortHeight * (ViewPort.physicalWidth/ViewPort.physicalHeight)
            ViewPort.viewPortHeight = ViewPort.virtualHeight
        } else {
            ViewPort.viewPortWidth = ViewPort.virtualWidth
            ViewPort.viewPortHeight = ViewPort.viewPortWidth * (ViewPort.physicalHeight / ViewPort.physicalWidth)
        }

        Gdx.app.debug(TAG, "WorldRenderer: virtual (${ViewPort.virtualWidth}, ${ViewPort.virtualHeight})")
        Gdx.app.debug(TAG, "WorldRenderer: viewport (${ViewPort.viewPortWidth}, ${ViewPort.viewPortHeight})")
        Gdx.app.debug(TAG, "WorldRenderer: physical (${ViewPort.physicalWidth}, ${ViewPort.physicalHeight})")
    }
}