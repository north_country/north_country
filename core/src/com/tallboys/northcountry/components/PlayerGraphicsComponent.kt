package com.tallboys.northcountry.components

import arrow.core.Some
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.JsonValue
import com.tallboys.northcountry.AnimationConfig
import com.tallboys.northcountry.Entity
import com.tallboys.northcountry.MapManager
import com.tallboys.northcountry.components.Component.Companion.MESSAGE_TOKEN

class PlayerGraphicsComponent: GraphicsComponent() {
    private var previousPosition = Vector2()

    override fun update(entity: Entity, mapManager: MapManager, batch: Batch, delta: Float) {
        updateAnimations(delta)

        if (previousPosition.x != currentPosition.x ||
                previousPosition.y != currentPosition.y) {
            previousPosition = currentPosition.cpy()
        }

        mapManager.camera.position.set(currentPosition.x, currentPosition.y, 0f)
        mapManager.camera.update()

        batch.begin()
        batch.draw(currentFrame, currentPosition.x, currentPosition.y, 4f, 4f)
        batch.end()

    }

    override fun dispose() {

    }

    override fun receiveMessage(message: String) {
        val string = message.split(MESSAGE_TOKEN)

        if (string[0] == Component.MESSAGE.CURRENT_POSITION.toString() ||
            string[0] == Component.MESSAGE.INIT_START_POSITION.toString()) {
            val x = string[1].toFloat()
            val y = string[2].toFloat()
            currentPosition.set(x, y)
        } else if (string[0] == Component.MESSAGE.CURRENT_STATE.toString()) {
            currentState = Entity.State.valueOf(string[1])
        } else if (string[0] == Component.MESSAGE.CURRENT_DIRECTION.toString()) {
            currentDirection = Entity.Direction.valueOf(string[1])
        }else if (string[0] == Component.MESSAGE.LOAD_ANIMATIONS.toString()) {
            val animationList: Array<JsonValue> = json.fromJson(Array::class.java, Gdx.files.internal("sprites/animations/player.json"))
                    as Array<JsonValue>

            animationList.forEach {
                val config = json.readValue(AnimationConfig::class.java, it)
                when (val animation = loadAnimation(config.texturePath, config.gridPoints, config.frameDuration)) {
                    is Some -> animations[config.animationType] = animation.value
                }
            }

        }
    }
}