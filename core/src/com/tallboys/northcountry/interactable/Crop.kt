package com.tallboys.northcountry.interactable

import com.badlogic.gdx.math.Rectangle
import com.tallboys.northcountry.MapManager

class Crop(rectangle: Rectangle, val mapManager: MapManager): Interactive(rectangle) {

    override fun interact() {
        // todo testing only, obviously we would check to see if we can harvest
        mapManager.notify(MapManager.MapMessage.HARVEST_CROP, this, "tomato")
    }
}