package com.tallboys.northcountry.components

import arrow.core.Some
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.maps.objects.RectangleMapObject
import com.tallboys.northcountry.Entity
import com.tallboys.northcountry.maps.MapFactory
import com.tallboys.northcountry.MapManager
import com.tallboys.northcountry.components.Component.Companion.MESSAGE_TOKEN
import com.tallboys.northcountry.maps.Map.Companion.PLAYER_START

class PlayerPhysicsComponent: PhysicsComponent() {
    companion object {
        private const val TAG = "PLAYER PHYSICS COMPONENT"
    }

    var interacting = false

    init {
        initBoundingBox(0.3f, 0.5f)
    }
    override fun update(entity: Entity, mapManager: MapManager, delta: Float) {
        updateBoundingBoxPosition(nextEntityPosition)
        checkPlayerSpawn(mapManager)
        if (interacting) {
            checkPlayerInteractive(mapManager)
            interacting = false
        }

        if (!isCollisionWithMapLayer(entity, mapManager) &&
            (state == Entity.State.WALKING || state == Entity.State.RUNNING)) {
            setNextPositionToCurrent(entity)

        } else {
            updateBoundingBoxPosition(currentEntityPosition)
        }
        calculateNextPosition(delta)
    }

    override fun dispose() {
    }

    override fun receiveMessage(message: String) {
        val string = message.split(MESSAGE_TOKEN)

        if (string[0] == Component.MESSAGE.INIT_START_POSITION.toString()) {
            if (string.size == 3) {
                val x = string[1].toFloat()
                val y = string[2].toFloat()
                currentEntityPosition.set(x, y)
                nextEntityPosition.set(currentEntityPosition)
            } else {
                Gdx.app.debug(TAG, "Init start position message invalid")
            }
        } else if (string[0] == Component.MESSAGE.CURRENT_STATE.toString()) {
            state = Entity.State.valueOf(string[1])
        } else if (string[0] == Component.MESSAGE.CURRENT_DIRECTION.toString()) {
            currentDirection = Entity.Direction.valueOf(string[1])
        } else if (string[0] == Component.MESSAGE.COLLISION_WITH_INTERACTIVE.toString()) {
            interacting = true
        }
    }

    private fun checkPlayerSpawn(mapManager: MapManager) {
        when (val spawn = mapManager.map.getSpawnLayer()) {
            is Some -> {
                spawn.value.objects.forEach {
                    if (it is RectangleMapObject && !it.name.contains(PLAYER_START)) {
                        if (boundingBox.overlaps(it.rectangle)) {
                            mapManager.loadMap(MapFactory.MapType.valueOf(it.name))
                        }
                    }

                }
            }
        }
    }

    private fun checkPlayerInteractive(mapManager: MapManager) {
        mapManager.getInteractiveObjects().forEach {
            if (boundingBox.overlaps(it.rectangle)) {
                Gdx.app.debug(TAG, "interacting...")
                it.interact()
            }
        }
    }
}